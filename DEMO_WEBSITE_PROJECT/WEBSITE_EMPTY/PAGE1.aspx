﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="PAGE1.aspx.cs" Inherits="PAGE1" %>

<%@ Register Src="~/Login.ascx" TagPrefix="uc1" TagName="Login" %>

<asp:Content ID="head" ContentPlaceHolderID="cphHead" runat="Server" ClientIDMode="Static">
    <script>
        function checkInput() {
            var result = false;
            try {
                var username = document.getElementById('txtUsername').value;

                var password = document.getElementById('txtPassword').value;

                if (username !== '' && password !== '') {
                    result = true;
                }
                else {
                    alert('Username and Password is required.');
                }
            }
            catch (e) {
                alert(e);
            }

            return result;
        }
    </script>
</asp:Content>
<asp:Content ID="body" ContentPlaceHolderID="cphBody" runat="Server" ClientIDMode="Static">
    PAGE 1
    <br />
    <%--<uc1:Login runat="server" ID="Login" />--%>
Username:
    <asp:TextBox ID="txtUsername" runat="server" onchange=""></asp:TextBox>
    <br />
    <br />
    Password:
    <asp:TextBox ID="txtPassword" runat="server"></asp:TextBox>
    <br />
    <br />
    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClientClick="return checkInput();" OnClick="AuthenticateUser" />
    <br />
    <br />
    <asp:Label ID="lblAlert" runat="server" ForeColor="Red"></asp:Label>
</asp:Content>