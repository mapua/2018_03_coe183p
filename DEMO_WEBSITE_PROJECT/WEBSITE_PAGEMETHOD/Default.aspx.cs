﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using Newtonsoft.Json;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod]
    public static string GetData(string f, string s)
    {
        Person person = new Person();

        person.FirstName = f;

        person.LastName = s;

         //throw new Exception("Custom Error :) ");

        return JsonConvert.SerializeObject(person);
    }

    [WebMethod]
    public static string GetDate()
    {
        return DateTime.Now.ToString();
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        Person person = new Person();

        person.FirstName = this.txtFName.Text;

        person.LastName = this.txtSName.Text;

        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "TEST", "alert('" + person.LastName +"')", true);
        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "TEST", "alert('" + person.FirstName + "')", true);
        
    }
}