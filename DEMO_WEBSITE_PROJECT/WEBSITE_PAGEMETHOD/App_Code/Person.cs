﻿/// <summary>
/// Summary description for Person
/// </summary>
public class Person
{
    private string firstName = string.Empty;

    private string lastName = string.Empty;

    public string FirstName
    {
        get { return firstName; }

        set { firstName = value; }
    }

    public string LastName
    {
        get { return lastName; }

        set { lastName = value; }
    }
}