﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PageMethod and JSON Sample</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript">
        function OnRequestComplete(result, userContext, methodName) {
            var Person = eval('(' + result + ')');
            alert(Person.LastName);
            alert(Person.FirstName);
        }

        function OnRequestError(error, userContext, methodName) {
            if (error != null) {
                alert(error.get_message());
            }
        }

        function SubmitData() {
            var f = document.getElementById('txtFName').value;
            var s = document.getElementById('txtSName').value;
            PageMethods.GetData(f, s, OnRequestComplete, OnRequestError);
        }

        function SubmitDataJQUERY() {
            var person = {
                f: $("#txtFName").val(),
                s: $("#txtSName").val()
            }

            $.ajax({
                type: "POST",
                url: "Default.aspx/GetData",
                data: JSON.stringify(person),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    var person = JSON.parse(msg.d);
                    alert(person.FirstName);
                    alert(person.LastName);
                }
            });
        }

        function GetDateTime(){
            $.ajax({
                type: "POST",
                url: "Default.aspx/GetDate",
                data: "{}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    alert(msg.d);
                }
            });
        }
    </script>
</head>
<body >
    <form id="form1" runat="server">
        <asp:ScriptManager ID="scriptManager" runat="server" EnablePageMethods="true"></asp:ScriptManager>
        <div>
            First Name:
            <asp:TextBox ID="txtFName" runat="Server"></asp:TextBox><br />
            Last Name:
            <asp:TextBox ID="txtSName" runat="Server"></asp:TextBox><br />
            <input type="button" value="Submit" onclick="SubmitDataJQUERY()" />
            <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
        </div>
    </form>77
</body>
</html>