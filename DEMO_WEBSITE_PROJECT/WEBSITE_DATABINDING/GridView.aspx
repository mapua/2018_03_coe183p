﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.master" AutoEventWireup="true" CodeFile="GridView.aspx.cs" Inherits="DataBinding_GridView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cpHead" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cpBody" runat="Server">
    <asp:DropDownList ID="cboUserType" runat="server" OnSelectedIndexChanged="cboUserType_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
    <br />
    Privilege Level:
    <asp:Label ID="lblPrivilege" runat="server"></asp:Label>
    <br />
    <%--<asp:GridView ID="gvUsers" runat="server"></asp:GridView>--%>
    <asp:GridView ID="gvUsers" runat="server" AutoGenerateColumns="false" EnableViewState="True" DataKeyNames="User_ID, User_Type_Code" AllowPaging="True" PageSize="5" BorderColor="Yellow">
        <Columns>
            <asp:BoundField HeaderText="First Name" DataField="FirstName" />
            <asp:BoundField HeaderText="Last Name" DataField="LastName" />
            <asp:BoundField HeaderText="User Type" DataField="Description" />
            <asp:TemplateField>
                <HeaderTemplate>
                    Gender
                </HeaderTemplate>
                <HeaderStyle Width="80px" />
                <ItemTemplate>
                    <asp:Label runat="server"
                        Text='<%# Eval("Gender").ToString().ToUpper() == "M" ? "MALE" : "FEMALE"%>'>
                    </asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>