﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _Default : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if(!this.IsPostBack)
        {
            this.LoadUserTypes();
        }
        else
        {
            //if (ViewState["User_Types"] != null)
            //{
            //    this.cboUserType.DataSource = (DataTable)ViewState["User_Types"];
            //    this.cboUserType.DataBind();
            //    this.cboUserType.DataTextField = "Description";
            //    this.cboUserType.DataValueField = "User_Type_Code";
            //}
        }
    }

    private void LoadUserTypes()
    {
        this.cboUserType.DataSource = null;
        this.cboUserType.DataBind();

        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLDB"].ConnectionString);
        string query = "SELECT User_Type_Code, [Description] FROM User_Type;";
        SqlCommand command = new SqlCommand(query, connection);
        SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
        DataTable userTypes = new DataTable();
        
        try
        {
            dataAdapter.Fill(userTypes);
            ViewState["User_Types"] = userTypes;
            this.cboUserType.DataSource = userTypes;
            this.cboUserType.DataTextField = "Description";
            this.cboUserType.DataValueField = "User_Type_Code";
            this.cboUserType.DataBind();
        }
        catch(Exception ex)
        {

        }
    }

    private void LoadUsers(int userTypeCode)
    {
        this.gvUsers.DataSource = null;
        this.gvUsers.DataBind();

        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLDB"].ConnectionString);
        string commandText =
        "SELECT\n" +
        "    U.User_ID,\n" +
        "    U.LastName,\n" +
        "    U.FirstName,\n" +
        "    U.Gender,\n" +
        "    UT.User_Type_Code,\n" +
        "    UT.[Description]\n" +
        "FROM Users AS U WITH (NOLOCK)\n" +
        "INNER JOIN User_Type AS UT WITH (NOLOCK)\n" +
        "    ON UT.User_Type_Code = U.User_Type_Code\n" +
        "WHERE U.User_Type_Code = @User_Type_Code;";

        SqlCommand command = new SqlCommand(commandText, connection);
        command.Parameters.Add("@User_Type_Code", SqlDbType.Int).Value = userTypeCode;
        SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
        DataTable users = new DataTable();

        try
        {
            dataAdapter.Fill(users);
            this.gvUsers.DataSource = users;
            this.gvUsers.DataBind();
        }
        catch (Exception ex)
        {

        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        this.LoadUsers(Convert.ToInt32(this.cboUserType.SelectedValue));
    }
}