﻿/// <summary>
/// Summary description for UserTypes
/// </summary>
public class UserType
{
    public int UserTypeCode { get; set; }
    public string Description { get; set; }
}