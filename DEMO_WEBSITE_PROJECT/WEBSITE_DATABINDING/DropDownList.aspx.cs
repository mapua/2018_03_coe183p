﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;

/// <summary>
/// dfbxdrhsersenhr
/// </summary>
public partial class DataBinding_DropDownList : System.Web.UI.Page
{
    private int userID = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        //Dictionary<string> userTypes = new List<string>();
        //userTypes.Add("")
        //Dictionary<string, string> userTypes = new Dictionary<string, string>();
        //userTypes.Add("1", "ADMINISTRATOR");

        //this.cboUserType.Items.Add("ADMINISTRATOR");
        //this.cboUserType.Items.Add(new System.Web.UI.WebControls.ListItem() { Text="Administrator", Value="1"});
        if (!Page.IsPostBack)
        {
            this.LoadUserTypes();

            //List<UserType> userTypes = new List<UserType>();
            //userTypes.Add(new UserType { UserTypeCode = 1, Description = "ADMINISTRATOR" });
            //userTypes.Add(new UserType { UserTypeCode = 2, Description = "MEMBER" });
            //userTypes.Add(new UserType { UserTypeCode = 3, Description = "GUEST" });

            //this.cboUserType.DataSource = null;

            //this.cboUserType.DataSource = userTypes;
            //this.cboUserType.DataTextField = "Description";
            //this.cboUserType.DataValueField = "UserTypeCode";
            //this.cboUserType.DataBind();
        }
    }

    private void LoadUserTypes()
    {
        this.cboUserType.DataSource = null;
        this.cboUserType.DataBind();

        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLDB"].ConnectionString);
        string query = "procGetUserTypes";
        SqlCommand command = new SqlCommand(query, connection);
        command.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
        DataTable userTypes = new DataTable();

        try
        {
            dataAdapter.Fill(userTypes);
            ViewState["User_Types"] = userTypes;
            this.cboUserType.DataSource = userTypes;
            this.cboUserType.DataTextField = "Description";
            this.cboUserType.DataValueField = "User_Type_Code";
            this.cboUserType.DataBind();
        }
        catch (Exception ex)
        {
        }
    }

    protected void cboUserType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ViewState["User_Types"] != null)
        {
            DataTable userTypes = (DataTable)ViewState["User_Types"];
            
            foreach(DataRow userType in userTypes.Select("User_Type_Code = " + this.cboUserType.SelectedValue.ToString()))
            {
                this.lblPrivilege.Text = userType["Privilege_Level"].ToString();
            }
        }
    }
    protected void btnOk_Click(object sender, EventArgs e)
    {

    }
}