﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web.UI;

public partial class DataBinding_GridView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            this.LoadUserTypes();
            this.LoadUsers(Convert.ToInt32(this.cboUserType.SelectedValue));

            /*List<UserType> userTypes = new List<UserType>();
            userTypes.Add(new UserType { UserTypeCode = 1, Description = "ADMINISTRATOR" });
            userTypes.Add(new UserType { UserTypeCode = 2, Description = "MEMBER" });
            userTypes.Add(new UserType { UserTypeCode = 3, Description = "GUEST" });

            this.cboUserType.DataSource = null;

            this.cboUserType.DataSource = userTypes;
            this.cboUserType.DataTextField = "Description";
            this.cboUserType.DataValueField = "UserTypeCode";
            this.cboUserType.DataBind();*/
        }
    }

    private void LoadUserTypes()
    {
        this.cboUserType.DataSource = null;
        this.cboUserType.DataBind();

        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLDB"].ConnectionString);
        string query = "procGetUserTypes";
        SqlCommand command = new SqlCommand(query, connection);
        command.CommandType = CommandType.StoredProcedure;
        SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
        DataTable userTypes = new DataTable();

        try
        {
            dataAdapter.Fill(userTypes);
            ViewState["User_Types"] = userTypes;
            this.cboUserType.DataSource = userTypes;
            this.cboUserType.DataTextField = "Description";
            this.cboUserType.DataValueField = "User_Type_Code";
            this.cboUserType.DataBind();
        }
        catch (Exception ex)
        {
        }
    }

    private void LoadUsers(int userTypeCode)
    {
        this.gvUsers.DataSource = null;
        this.gvUsers.DataBind();

        SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["SQLDB"].ConnectionString);
        string commandText = "procGetUsersByUserType";

        SqlCommand command = new SqlCommand(commandText, connection);
        command.CommandType = CommandType.StoredProcedure;
        command.Parameters.Add("@User_Type_Code", SqlDbType.Int).Value = userTypeCode;
        SqlDataAdapter dataAdapter = new SqlDataAdapter(command);
        DataTable users = new DataTable();

        try
        {
            dataAdapter.Fill(users);
            this.gvUsers.DataSource = users;
            this.gvUsers.DataBind();
            //this.gvUsers.Columns[0].Visible = false;
            //this.gvUsers.Columns[4].Visible = false;
        }
        catch (Exception ex)
        {
        }
    }

    protected void cboUserType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ViewState["User_Types"] != null)
        {
            DataTable userTypes = (DataTable)ViewState["User_Types"];

            foreach (DataRow userType in userTypes.Select("User_Type_Code = " + this.cboUserType.SelectedValue.ToString()))
            {
                this.lblPrivilege.Text = userType["Privilege_Level"].ToString();
            }
        }

        this.LoadUsers(Convert.ToInt32(this.cboUserType.SelectedValue));
    }
}