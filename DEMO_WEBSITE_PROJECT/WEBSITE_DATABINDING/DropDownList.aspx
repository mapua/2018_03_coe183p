﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="DropDownList.aspx.cs" Inherits="DataBinding_DropDownList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:DropDownList ID="cboUserType" runat="server" OnSelectedIndexChanged="cboUserType_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
            <br />
            Privilege Level: <asp:Label ID="lblPrivilege" runat="server"></asp:Label>
            <%--<asp:Button ID="btnOk" runat="server" Text="OK" OnClick="btnOk_Click"/>--%>
        </div>
    </form>
</body>
</html>