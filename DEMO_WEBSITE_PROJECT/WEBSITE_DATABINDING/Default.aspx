﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:DropDownList ID="cboUserType" runat="server"></asp:DropDownList>
            <br />
            <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
            <asp:GridView ID="gvUsers" runat="server" AutoGenerateColumns="False" EnableViewState="True" DataKeyNames="User_ID, User_Type_Code" AllowPaging="True" PageSize="5">
                <Columns>
                    <asp:BoundField HeaderText="First Name" DataField="FirstName" />
                    <asp:BoundField HeaderText="Last Name" DataField="LastName" />
                    <asp:BoundField HeaderText="User Type" DataField="Description" />
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Gender
                        </HeaderTemplate>
                        <HeaderStyle Width="80px" />
                        <ItemTemplate>
                            <asp:Label runat="server"
                                Text='<%# Eval("Gender")=="M" ? "MALE" : "FEMALE "%>'>
                            </asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
    </form>
</body>
</html>
