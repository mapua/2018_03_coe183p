﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="UpdatePanel.aspx.cs" Inherits="UpdatePanel_UpdatePanel" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePageMethods="True"></asp:ScriptManager>
        <asp:Button ID="btnGo" runat="server" Text="Go" OnClick="btnGo_Click" />
        <asp:UpdatePanel ID="upContainer" runat="server">
            <ContentTemplate>
                <br>
                <asp:Label ID="lblCurrentTime" runat="server" Text="Label"></asp:Label>
                <br />
                <asp:DropDownList ID="cboUserType" runat="server"></asp:DropDownList>
                <%--OnSelectedIndexChanged="cboUserType_SelectedIndexChanged" AutoPostBack="true"--%>
                <br />
                <asp:GridView ID="gvUsers" runat="server" AutoGenerateColumns="false" EnableViewState="True" DataKeyNames="User_ID, User_Type_Code" AllowPaging="True" PageSize="5">
                    <Columns>
                        <asp:BoundField HeaderText="First Name" DataField="FirstName" />
                        <asp:BoundField HeaderText="Last Name" DataField="LastName" />
                        <asp:BoundField HeaderText="User Type" DataField="Description" />
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Gender
                            </HeaderTemplate>
                            <HeaderStyle Width="80px" />
                            <ItemTemplate>
                                <asp:Label runat="server"
                                    Text='<%# Eval("Gender")=="M" ? "MALE" : "FEMALE "%>'>
                                </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="btnGo" EventName="Click" />
            </Triggers>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <br />
                Privilege Level:
                <asp:Label ID="lblPrivilege" runat="server"></asp:Label>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>