﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WEBFORMS.Startup))]
namespace WEBFORMS
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
