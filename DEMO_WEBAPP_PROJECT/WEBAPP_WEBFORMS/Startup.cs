﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(WEBAPP_WEBFORMS.Startup))]
namespace WEBAPP_WEBFORMS
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
